from flask import Flask, Response, render_template, request, jsonify
from redis import Redis, RedisError
import json
#from rejson import Client, Path

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

_MAX = 200
_BL_FIELDS = ["Caller_MSISDN", "Enforce_Flag", "Corporate_Flag", "From_Time", "To_Time", "RBT"]
_VPN_FIELDS = ["MSISDN", "Short Number", "Direction", "Member ID", "Roaming"]
_NNS_FIELDS = ["called_msisdn", "new_msisdn", "active", "start_date", "end_date"]

_NNS_DICT = {'called_msisdn': 0, 'new_msisdn': 1, 'active': 2, 'start_date': 3, 'end_date': 4}
_VPN_DICT = {"MSISDN": 0, "Short Number": 1, "Direction": 2, "Member ID": 3, "Roaming": 4}
_BL_DICT = {"Caller_MSISDN": 0, "Enforce_Flag": 1, "Corporate_Flag": 2, "From_Time": 3, "To_Time": 4, "RBT": 5}

_MSISDN_LENGTH = 9


def check_vpn_id(_id):
    numbers = [i[10:] for i in redis.keys('VPN_GROUP_*')]
    if _id in numbers:
        return True
    else:
        return False


# This method assumes each msisdn is unique and can be a member of a single VPN only
def find_vpn_by_msisdn(msisdn):
    all_VPNs = [i for i in redis.keys('VPN_GROUP_*')]
    for VPN_ID in all_VPNs:
        data = redis.smembers(VPN_ID)
        if msisdn in str(data):
            return VPN_ID
    return None


def is_valid_msisdn(msisdn):
    if msisdn is None:
        return False
    elif msisdn == '':
        return False
    elif len(msisdn) > 15 or len(msisdn) < 7:
        return False
    else:
        return True


def get_available_vpn_ids():
    numbers = [int(i[10:]) for i in redis.keys('VPN_GROUP_*')]
    available_ids = [i for i in range(1, _MAX + 1) if i not in numbers]
    return available_ids


def get_all_vpn_sets():
    all_VPN_keys = [i for i in redis.keys('VPN_GROUP_*')]
    all_VPNs = [list(redis.smembers(vpn_set)) for vpn_set in all_VPN_keys]
    return all_VPNs


def check_mo_blacklisted(mo, mt):
    # Check if the mobile originator is blacklisted by the mobile terminator (args are msisdns)
    if redis.smembers('BL_%s' % mt):
        for member in redis.smembers('BL_%s' % mt):
            if mo == member.split()[_BL_DICT["Caller_MSISDN"]]:
                if member.split()[_BL_DICT["Enforce_Flag"]] == '0':
                    return {'1': "Mobile Originator is Blacklisted"}
                else:
                    return {'0': "Mobile Originator uses Enforce Flag"}
        return {'0': "Mobile Originator is not Blacklisted"}
    return {'0': "Mobile Originator is not Blacklisted"}


@app.route('/')
def hello():
    redis.incr('hits')
    return 'Hello World! I have been seen %s times.' % redis.get('hits')


@app.route('/keys')
def get_keys_by_pattern_param():
    pattern = request.args.get('pattern')
    return str(redis.keys(pattern))


@app.route('/get/<key>')
@app.route('/get/<key>/')
def get_key_val(key):
    if redis.get(key):
        return redis.get(key)
    else:
        return "Error Occurred"


@app.route('/api/get/<key>')
@app.route('/api/get/<key>/')
def get_key_val_api(key):
    try:
        if redis.get(key):
            return Response(json.dumps({key: redis.get(key)}),  mimetype='application/json')
    except:
        try:
            if redis.hgetall(key):
                return Response(json.dumps(redis.hgetall(key)), mimetype='application/json')
        except:
            if redis.smembers(key):
                dct_smembers = {k: v for k, v in enumerate(redis.smembers(key))}
                return Response(json.dumps(dct_smembers), mimetype='application/json')
    return Response(json.dumps({'message': 'Key %s not found' % key}), mimetype='application/json')


@app.route('/api/delete/<key>')
@app.route('/api/delete/<key>/')
def remove_key(key):
    if redis.delete(key) > 0:
        return Response(json.dumps({'message': 'Key %s deleted successfully' % key}), mimetype='application/json')
    else:
        return Response(json.dumps({'message': 'Key %s not found' % key}), mimetype='application/json')


@app.route('/set/<key>/<value>')
def set_key_val(key, value):
    if redis.set(key, value):
        return "Key '{}' is set with value '{}'".format(key, value)
    else:
        return "Error Occurred"


@app.route('/set/hash')
@app.route('/set/hash/')
def set_hash():
    return render_template('hashmap.html')


@app.route('/set/hash', methods=['POST'])
@app.route('/set/hash/', methods=['POST'])
def save_hash_by_key():
    key = request.form['key']
    field = request.form['field']
    value = request.form['value']
    field2 = request.form['field2']
    value2 = request.form['value2']
    if redis.hset(key, field, value) and redis.hset(key, field2, value2):
        return render_template('hashmap_2.html')
    else:
        return "Error Occurred"


@app.route('/get_hash')
def get_hash_members():
    key = request.args.get('key')
    if redis.hgetall(key):
        return redis.hgetall(key)
    else:
        return "Invalid or empty HashMap"


@app.route('/api/get_hash/<key>')
@app.route('/api/get_hash/<key>/')
def get_hash_members_api(key):
    if redis.hgetall(key):
        return Response(json.dumps(redis.hgetall(key)), mimetype='application/json')
    else:
        return Response(json.dumps({'message': 'Key %s not found' % key}), mimetype='application/json')


@app.route('/add_to_set/<key>/<values>')
@app.route('/add_to_set/<key>/<values>/')
def add_to_set(key, values):
    if redis.sadd(key, values):
        return "'{}' values were added to a Set whose key is '{}'".format(values, key)
    else:
        return "Duplicate values aren't allowed in Sets"


@app.route('/get_a_set/<key>')
@app.route('/get_a_set/<key>/')
def print_set(key):
    if redis.smembers(key):
        data = redis.smembers(key)
        return render_template('set_display.html', data=data)
    else:
        return "Invalid or empty Set entered"


@app.route('/vpn/get_fields/<_id>')
@app.route('/vpn/get_fields/<_id>/')
def print_fields(_id):
    if redis.smembers('VPN_GROUP_%s' % _id):
        all_members = ' ,'.join(redis.smembers('VPN_GROUP_%s' % _id))
        return render_template("vpn_display.html", vpn=all_members)


@app.route('/api/vpn/get_fields', methods=['GET'])
def get_vpn_fields():
    _id = request.args.get('vpn_group_id')
    if redis.smembers('VPN_GROUP_%s' % _id):
        all_members = redis.smembers('VPN_GROUP_%s' % _id)
        lst = []
        for record in all_members:
            d = {key: '' for key in _VPN_FIELDS}
            for i, field_name in enumerate(_VPN_FIELDS):
                d[field_name] = record.split()[i]
            lst.append(d)
        return Response(json.dumps(lst), mimetype='application/json')
    else:
        return Response("Selected VPN doesn't exist", mimetype='application/json')


@app.route('/remove_from_vpn/<_id>/<msisdn>')
@app.route('/remove_from_vpn/<_id>/<msisdn>/')
def find_and_remove(_id, msisdn):
    lst = list(redis.smembers('VPN_GROUP_%s' % (_id)))
    for record in lst:
        if msisdn in record:
            redis.srem('VPN_GROUP_%s' % (_id), record)
            return 'MSISDN %s removed from VPN' % (msisdn)
    return "MSISDN not found in VPN GROUP %s" % (_id)


@app.route('/vpn/get_msisdns/<_id>')
@app.route('/vpn/get_msisdns/<_id>/')
def get_vpn_msdisdns(_id):
    if redis.smembers('VPN_GROUP_%s' % (_id)):
        msisdns = ''
        for member in redis.smembers('VPN_GROUP_%s' % (_id)):
                msisdns += member[0:11]
        lst = list(msisdns.split(' ')[:-1])
        return render_template("list_msisdns_by_vpn.html", lst=lst, id=_id)
    else:
        return "Invalid or empty VPN"


@app.route('/api/vpn/get/msisdns', methods=['GET'])
def get_vpn_msdisdns_api():
    _id = request.args.get('vpn_group_id')
    if redis.smembers('VPN_GROUP_%s' % _id):
        lst = []
        for member in redis.smembers('VPN_GROUP_%s' % _id):
            lst.append({'msisdn': member[0:11], 'member_id': member[17:19].strip()})
        return Response(json.dumps(lst), mimetype='application/json')
    else:
        return "Invalid or empty VPN"


@app.route('/vpn/get_short_numbers/<_id>')
@app.route('/vpn/get_short_numbers/<_id>/')
def get_vpn_short_numbers(_id):
    if redis.smembers('VPN_GROUP_%s' % _id):
        short_nums = ''
        for member in redis.smembers('VPN_GROUP_%s' % _id):
            if member[0] != '0':
                short_nums += member[12:16] + ' '
            else:
                short_nums += member[11:15] + ' '
        return short_nums
    else:
        return "Invalid or empty VPN"
    

@app.route('/vpn')
@app.route('/vpn/')
def set_new_vpn():
    return render_template('new_vpn.html')


@app.route('/vpn/<key>')
def check_vpn(key):
    if check_vpn_id(key):
        return "VPN Exists"
    else:
        numbers = [i[10:] for i in redis.keys('VPN_GROUP_*')]
        return str(numbers)


@app.route('/vpn', methods=['POST'])
@app.route('/vpn/', methods=['POST'])
def add_to_vpn():
    # add validation for key and other fields
    key = request.form['key']
    lst = [request.form['msisdn'], request.form['short_number'], request.form['direction'], request.form['member_id'], request.form['roaming']]
    str_representation = ' '.join(lst) 
    if redis.sadd('VPN_GROUP_%s' % key, str_representation):
        return render_template('new_vpn_continue.html', data=key, lst=get_available_vpn_ids())
    else:
        return "Error occurred"


@app.route('/api/vpn', methods=['POST'])
@app.route('/api/vpn/', methods=['POST'])
def add_to_vpn_api():
    req_data = request.get_json()
    key = req_data['id']
    lst = [str(req_data['msisdn']), str(req_data['short_number']), str(req_data['direction']), str(req_data['member_id']),
           str(req_data['roaming'])]
    assert len(lst) == 5, "Some field of VPN is missing, please fill all fields"
    str_representation = ' '.join(lst)
    if redis.sadd('VPN_GROUP_%s' % key, str_representation):
        return Response(json.dumps({'message': 'New VPN Group with Id %s created successfully' % key}),
                        mimetype='application/json')
    else:
        return Response(json.dumps({'message': 'Error occurred'}))


@app.route('/vpn/all')
@app.route('/vpn/all/')
def get_all_vpns():
    lst = redis.keys('VPN_GROUP_*')
    return render_template('list_vpns.html', data=lst)


@app.route('/vpn/all_msisdns')
@app.route('/vpn/all_msisdns/')
def list_all_vpn_msdisdns():
    lst = redis.keys('VPN_GROUP_*')
    dct = {}
    for vpn_group in lst:
        dct[vpn_group] = list(redis.smembers(vpn_group))
    return render_template('list_msisdns.html', dictionary=dct, vpn_groups=get_all_vpn_sets())


@app.route('/vpn/all_short_numbers')
@app.route('/vpn/all_short_numbers/')
def list_all_vpn_snumbers():
    lst = redis.keys('VPN_GROUP_*')
    dct = {}
    for vpn_group in lst:
        dct[vpn_group] = list(redis.smembers(vpn_group))
    return render_template('list_snumbers.html', dictionary=dct)


@app.route('/vpn/count/<_id>')
@app.route('/vpn/count/<_id>/')
def count_msisdns_in_vpn(_id):
    return "This VPN has " + str(redis.scard('VPN_GROUP_%s' % _id)) + " msisdns"


@app.route('/api/vpn/call/<mo>/<mt>')
@app.route('/api/vpn/call/<mo>/<mt>/')
def detect_related_msisdns(mo, mt):
    if is_valid_msisdn(mo) and is_valid_msisdn(mt):
        if find_vpn_by_msisdn(mo) == find_vpn_by_msisdn(mt) and find_vpn_by_msisdn(mo) is not None:
            return Response(json.dumps({'message': "Success! initializing internal VPN call"})
                            , mimetype='application/json')
        else:
            return Response(json.dumps({'message': "MSISDNs are not in the same VPN"}), mimetype='application/json')
    else:
        return Response(json.dumps({'message': "MSISDNs are not in the same VPN"}), mimetype='application/json')


@app.route('/fn')
@app.route('/fn/')
def set_new_fn():
    return render_template('fn.html')


@app.route('/fn', methods=['POST'])
@app.route('/fn/', methods=['POST'])
def add_to_fn():
    # add validation for selected msisdn
    msisdn = request.form['msisdn']
    lst = [request.form['FN1'], request.form['FN2'], request.form['FN3'], request.form['FN4'],
           request.form['FN5']]
    str_representation = ' '.join(lst)
    key = 'FN_{}'.format(msisdn, str_representation)
    if redis.sadd(key, str_representation):
        # need to add reference of the FN to the msisdn
        return "FN for msisdn %s created" % msisdn
    else:
        return "Error Occurred"


@app.route('/bl')
@app.route('/bl/')
def set_new_blacklist():
    return render_template('new_blacklist.html')


@app.route('/bl', methods=['POST'])
@app.route('/bl/', methods=['POST'])
def add_to_blacklist():
    # add validation for key and other fields
    key = "BL_%s" % request.form['msisdn']
    lst = [request.form[field] for field in _BL_FIELDS]
    str_representation = ' '.join(lst)
    if redis.sadd(key, str_representation):
        return "New Black List created for MSISDN %s " % request.form['msisdn']
    else:
        return "Error Occurred"


@app.route('/bl/get_fields/<msisdn>')
@app.route('/bl/get_fields/<msisdn>/')
def print_fields_bl(msisdn):
    if redis.smembers('BL_%s' % msisdn):
        all_members = ' ,'.join(redis.smembers('BL_%s' % msisdn))
        return render_template("bl_display.html", bl_properties=all_members)


'''
The following method return a dictionary in order to comply with JSON format of web APIs
'''
@app.route('/bl/callersdemo/<msisdn>')
@app.route('/bl/callersdemo/<msisdn>/')
def get_caller_msisdn(msisdn):
    # query a REDIS SET by the msisdn arg
    if redis.smembers('BL_%s' % msisdn):
        all_MSISDN_Callers = redis.smembers('BL_%s' % msisdn)
        return {'msisdn_%s' % (i + 1): s.split()[_BL_DICT["Caller_MSISDN"]] for i, s in enumerate(all_MSISDN_Callers)}
    else:
        return "Selected MSISDN has no black list"


@app.route('/bl/callers/<msisdn>')
@app.route('/bl/callers/<msisdn>/')
def get_caller_msisdns(msisdn):
    if redis.smembers('BL_%s' % msisdn):
        all_MSISDN_Callers = redis.smembers('BL_%s' % msisdn)
        counter = 1
        d = {"Caller_MSISDN_%s" % counter: '', "From_Time_%s" % counter: '', "To_Time_%s" % counter: ''}
        for record in all_MSISDN_Callers:
            d["Caller_MSISDN_%s" % counter] = record.split()[_BL_DICT["Caller_MSISDN"]]
            d["From_Time_%s" % counter] = record.split()[_BL_DICT["From_Time"]]
            d["To_Time_%s" % counter] = record.split()[_BL_DICT["To_Time"]]
            counter = counter + 1
        return d
    else:
        return "Selected MSISDN has no black list"


@app.route('/api/bl/get/<msisdn>', methods=['GET'])
@app.route('/api/bl/get/<msisdn>/', methods=['GET'])
def get_bl(msisdn):
    if redis.smembers('BL_%s' % msisdn):
        all_MSISDN_Callers = redis.smembers('BL_%s' % msisdn)
        lst = []
        for record in all_MSISDN_Callers:
            d = {key: '' for key in _BL_FIELDS}
            for i, field_name in enumerate(_BL_FIELDS):
                d[field_name] = record.split()[i]
            lst.append(d)
        return Response(json.dumps(lst),  mimetype='application/json')
    else:
        return Response(json.dumps({"message": "Selected MSISDN has no black list"}), mimetype='application/json')


@app.route('/api/bl/frbt', methods=['POST'])
@app.route('/api/bl/frbt/', methods=['POST'])
def set_frbt():
    req_data = request.get_json()
    rbt_id = "RBT_" + str(req_data["id"])
    if redis.keys(rbt_id):
        return Response(json.dumps({'message': 'FRBT with this ID already exists'}), mimetype='application/json')
    d = {"name": req_data["name"], "artist": req_data["artist"], "category": req_data["category"],
         "file_location": req_data["file_location"]}
    if redis.hset(rbt_id, "name", d["name"]):
        for k, v in d.items():
            redis.hset(rbt_id, k, v)
        return Response(json.dumps({'message': "FRBT set successfully"}),  mimetype='application/json')
    else:
        return Response(json.dumps({'message': "Error Occurred"}),  mimetype='application/json')


@app.route('/api/bl/frbt', methods=['PUT'])
@app.route('/api/bl/frbt/', methods=['PUT'])
def update_frbt():
    req_data = request.get_json()
    rbt_id = "RBT_" + str(req_data["id"])
    d = {"name": req_data["name"], "artist": req_data["artist"], "category": req_data["category"],
         "file_location": req_data["file_location"]}
    if not redis.hset(rbt_id, "name", d["name"]):
        for k, v in d.items():
            redis.hset(rbt_id, k, v)
        return Response(json.dumps({'message': "FRBT updated successfully"}),  mimetype='application/json')
    else:
        return Response(json.dumps({'message': "Error Occurred"}),  mimetype='application/json')


@app.route('/api/bl/frbt/<_id>', methods=['GET'])
@app.route('/api/bl/frbt/<_id>/', methods=['GET'])
def get_frbt(_id):
    if redis.hgetall("RBT_%s" % _id):
        return dict(redis.hgetall("RBT_%s" % _id))
    else:
        return Response(json.dumps({'message': "FRBT resource not found"}),  mimetype='application/json')


@app.route('/api/bl/get_all_frbt', methods=['GET'])
@app.route('/api/bl/get_all_frbt/', methods=['GET'])
def get_all_frbt_resources():
    rbt_ids = [rbt for rbt in redis.keys('RBT_*')]
    all_resources = {}
    for rbt_id in rbt_ids:
        all_resources[rbt_id] = get_frbt(rbt_id[4:])
    return all_resources


@app.route('/bl/<mo>/<mt>')
@app.route('/bl/<mo>/<mt>/')
def blah(mo, mt):
    return check_mo_blacklisted(mo, mt)


@app.route('/api/share_a_call', methods=['POST'])
@app.route('/api/share_a_call/', methods=['POST'])
def share_a_call():
    req_data = request.get_json()
    if req_data["who_pays"] not in [1, 2, 3, 4]:
        return {'0': 'Invalid payer value'}
    else:
        if req_data["who_pays"] == 4:
            return {'2': 'Payer MSISDN has to be defined'}
        elif req_data["who_pays"] == 3:
            payer_msisdn = req_data["msisdn_1"] + " " + req_data["msisdn_2"]
        elif req_data["who_pays"] == 2:
            payer_msisdn = req_data["msisdn_2"]
        elif req_data["who_pays"] == 1:
            payer_msisdn = req_data["msisdn_1"]
        group_id = 'SHARE_GROUP_%s' % req_data["id"]
        msisdns = req_data["msisdn_1"] + ", " + req_data["msisdn_2"]
        if redis.hset(group_id, "msisdns", msisdns):
            redis.hset(group_id, "id", req_data["id"])
            redis.hset(group_id, "who_pays", req_data["who_pays"])
            redis.hset(group_id, "payer_msisdn", payer_msisdn)
            return {'1': 'Sharing group created'}
        else:
            return {'0': 'Error occurred'}


@app.route('/api/share_a_call/<_id>', methods=['GET'])
@app.route('/api/share_a_call/<_id>/', methods=['GET'])
def get_sharing_group(_id):
    saved_hash = redis.hgetall('SHARE_GROUP_%s' % _id)
    msisdns = saved_hash["msisdns"].split(", ")
    new_hash = {key: value for key, value in saved_hash.items() if key is not "msisdns"}
    new_hash["msisdns"] = {"msisdn_1": msisdns[0], "msisdn_2": msisdns[1]}
    return new_hash


@app.route('/api/new_number_service', methods=['POST'])
@app.route('/api/new_number_service/', methods=['POST'])
def set_new_number_service_record():
    req_data = request.get_json()
    key = 'NNS_%s' % req_data[_NNS_FIELDS[0]]  # called_msisdn field is unique
    lst = [req_data[field] for field in _NNS_FIELDS]
    str_representation = ' '.join(lst)
    if redis.sadd(key, str_representation):
        return Response(json.dumps({'message': 'New record of New Number service created for {} which starts at {}'
                                   .format(lst[0], lst[-2])}), mimetype='application/json')
    else:
        return Response(json.dumps({'message': 'Error Occurred'}))


@app.route('/api/new_number_service/<called_msisdn>')
@app.route('/api/new_number_service/<called_msisdn>/')
def get_new_number_service_record(called_msisdn):
    if redis.smembers('NNS_%s' % called_msisdn):
        all_members = redis.smembers('NNS_%s' % called_msisdn)
        lst = []
        for record in all_members:
            d = {key: '' for key in _NNS_FIELDS}
            for i, field_name in enumerate(_NNS_FIELDS):
                d[field_name] = record.split()[i]
            lst.append(d)
        return Response(json.dumps(lst), mimetype='application/json')
    else:
        return Response(json.dumps({'message': 'Selected MSISDN is not registered to New Number service'})
                        , mimetype='application/json')



# Alternative to /bl/ - implemented with Redis HSET, not in use
@app.route('/blist')
@app.route('/blist/')
def set_new_black_list():
    return render_template('new_blacklist.html')


@app.route('/blist', methods=['POST'])
@app.route('/blist/', methods=['POST'])
def add_blacklist():
    assert request.form['msisdn'], "Error in MSISDN value"
    name = "BList_%s" % request.form['msisdn']
    dct = {key: request.form[key] for key in _BL_FIELDS}
    for k, v in dct.iteritems():
        flag = redis.hset(name, k, v)
    if flag:
        return "Success"
    else:
        return "Error Occurred"


# Not in use
@app.route('/list/<key>')
@app.route('/list/<key>/')
def get_list_members(key):
    return render_template('display_blacklist.html', lst=redis.lrange(key, 0, -1))


@app.route('/share_a_call')
@app.route('/share_a_call/')
def sharecall():
    obj = {
        'answer': 42,
        'arr': [None, True, 3.14],
        'truth': {
            'coord': 'out there'
        }
    }
    return rj.jsonset('obj', Path.rootPath(), obj)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
