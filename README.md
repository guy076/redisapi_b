1. Run `docker-compose build' to build the containers specified in the docker-compose file.

2. Run `docker-compose up` to start the containers (add -d to run them in the background. Then run `docker-compose stop` when done.

3. Browse to http://localhost:5000 to see the response from the web app. 

4. .....

5. PROFIT!
